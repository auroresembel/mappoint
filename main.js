 

    // On initialise la latitude et la longitude de Paris (centre de la carte)
            var lat = 44.4475229;
            var lon = 1.441989;
            var macarte = null;
            var json = require('./buscahors.json'); 

      // Fonction d'initialisation de la carte
            function initMap() {
                // Créer l'objet "macarte" et l'insèrer dans l'élément HTML qui a l'ID "map"
                macarte = L.map('map').setView([lat, lon], 11);
                // Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
                L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
                    // Il est toujours bien de laisser le lien vers la source des données
                    attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
                    minZoom: 1,
                    maxZoom: 20
                }).addTo(macarte);
                 // Variable pour les données d'un point
                for(point in json) {
                    var marker = L.marker([json[point].stop_lat,  json[point].stop_lon]).addTo(macarte);          
                };
            }

            window.onload = function(){
                // Fonction d'initialisation qui s'exécute lorsque le DOM est chargé
                initMap(); 
            };
            
        
